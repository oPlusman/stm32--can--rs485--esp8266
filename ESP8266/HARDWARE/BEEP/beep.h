#ifndef __BEEP_H
#define __BEEP_H	 
#include "sys.h" 

//引脚定义
/*******************************************************/
//BEEP
#define BEEP_PIN                  GPIO_Pin_7                 
#define BEEP_GPIO_PORT            GPIOG                      
#define BEEP_GPIO_CLK             RCC_AHB1Periph_GPIOG


/************************************************************/

//LED端口定义
#define BEEP PGout(7)	// 蜂鸣器控制IO 

void BEEP_Init(void);//初始化		 				    
#endif

















