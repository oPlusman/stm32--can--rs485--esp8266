#include "beep.h" 


/*
	BEEP初始化函数
*/
void BEEP_Init(void)
{   
  GPIO_InitTypeDef  GPIO_InitStructure;

  RCC_AHB1PeriphClockCmd(BEEP_GPIO_CLK, ENABLE);
  

  GPIO_InitStructure.GPIO_Pin = BEEP_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;//下拉
  GPIO_Init(BEEP_GPIO_PORT, &GPIO_InitStructure);//初始化GPIO
	
  BEEP = 0;
	
}

/*
	使用例子
	key_scan(0);	

	if(keydown_data==KEY0_DATA)   //key0按下后马上执行相应代码
	{
	   
		LED0 = 0;

	}
	if(keyup_data==KEY1_DATA)     //key1按下抬起之后执行相应代码
	{
		LED0 = 1;

	}

	if(keyup_data==KEY2_DATA)  //key2长按1秒后执行相应代码 由于延时5ms扫描一次按键所以5ms*200=1S
	{
		
		BEEP = 1;

	}
	if(keyup_data==KEY3_DATA)  //key3长按2秒后执行相应代码 由于延时5ms扫描一次按键所以5ms*400=2S
	{
		
		BEEP = 0;

	}
	delay_ms(5);

*/




