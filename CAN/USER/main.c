#include "includes.h"

#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "beep.h"
#include "key.h"

#include "string.h"
#include "stdlib.h"  
#include "bsp_can.h"


__IO uint32_t flag = 0;		 //用于标志是否接收到数据，在中断函数中赋值
CanTxMsg TxMessage;			     //发送缓冲区
CanRxMsg RxMessage;				 //接收缓冲区


char can_sendbuf[10];

//任务1控制块
OS_TCB Task1_TCB;

void task1(void *parg);

CPU_STK task1_stk[128];			//任务1的任务堆栈，大小为128字，也就是512字节



//任务2控制块
OS_TCB Task2_TCB;

void task2(void *parg);

CPU_STK task2_stk[128];			//任务2的任务堆栈，大小为128字，也就是512字节'

//任务3控制块
OS_TCB Task3_TCB;

void task3(void *parg);

CPU_STK task3_stk[128];			//任务2的任务堆栈，大小为128字，也就是512字节

//任务4控制块
OS_TCB Task4_TCB;

void task4(void *parg);

CPU_STK task4_stk[128];			//任务2的任务堆栈，大小为128字，也就是512字节'

//任务5控制块
OS_TCB Task5_TCB;

void task5(void *parg);

CPU_STK task5_stk[128];			//任务2的任务堆栈，大小为128字，也就是512字节


//主函数
int main(void)
{
	OS_ERR err;

	systick_init();  													//时钟初始化
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);						//中断分组配置
	 
	usart_init(115200);	//串口初始化波特率为115200
	LED_Init();		  		//初始化与LED连接的硬件接口  
	BEEP_Init();
	KEY_Init();

	/*初始化can,在中断接收CAN数据包*/
	CAN_Config();
	
	
	BEEP = 1;				//鸣叫提示接入成功
	delay_ms(250);
	BEEP = 0;
	delay_ms(250);

	//OS初始化，它是第一个运行的函数,初始化各种的全局变量，例如中断嵌套计数器、优先级、存储器
	OSInit(&err);


	//创建任务1
	OSTaskCreate(	(OS_TCB *)&Task1_TCB,									//任务控制块，等同于线程id
					(CPU_CHAR *)"Task1",									//任务的名字，名字可以自定义的
					(OS_TASK_PTR)task1,										//任务函数，等同于线程函数
					(void *)0,												//传递参数，等同于线程的传递参数
					(OS_PRIO)6,											 	//任务的优先级6		
					(CPU_STK *)task1_stk,									//任务堆栈基地址
					(CPU_STK_SIZE)128/10,									//任务堆栈深度限位，用到这个位置，任务不能再继续使用
					(CPU_STK_SIZE)128,										//任务堆栈大小			
					(OS_MSG_QTY)0,											//禁止任务消息队列
					(OS_TICK)0,												//默认时间片长度																
					(void  *)0,												//不需要补充用户存储区
					(OS_OPT)OS_OPT_TASK_NONE,								//没有任何选项
					&err													//返回的错误码
				);


	//创建任务2
	OSTaskCreate(	(OS_TCB *)&Task2_TCB,									//任务控制块
					(CPU_CHAR *)"Task2",									//任务的名字
					(OS_TASK_PTR)task2,										//任务函数
					(void *)0,												//传递参数
					(OS_PRIO)6,											 	//任务的优先级7		
					(CPU_STK *)task2_stk,									//任务堆栈基地址
					(CPU_STK_SIZE)128/10,									//任务堆栈深度限位，用到这个位置，任务不能再继续使用
					(CPU_STK_SIZE)128,										//任务堆栈大小			
					(OS_MSG_QTY)0,											//禁止任务消息队列
					(OS_TICK)0,												//默认时间片长度																
					(void  *)0,												//不需要补充用户存储区
					(OS_OPT)OS_OPT_TASK_NONE,								//没有任何选项
					&err													//返回的错误码
				);
	//创建任务3
	OSTaskCreate(	(OS_TCB *)&Task3_TCB,									//任务控制块
					(CPU_CHAR *)"Task3",									//任务的名字
					(OS_TASK_PTR)task3,										//任务函数
					(void *)0,												//传递参数
					(OS_PRIO)6,											 	//任务的优先级7		
					(CPU_STK *)task3_stk,									//任务堆栈基地址
					(CPU_STK_SIZE)128/10,									//任务堆栈深度限位，用到这个位置，任务不能再继续使用
					(CPU_STK_SIZE)128,										//任务堆栈大小			
					(OS_MSG_QTY)0,											//禁止任务消息队列
					(OS_TICK)0,												//默认时间片长度																
					(void  *)0,												//不需要补充用户存储区
					(OS_OPT)OS_OPT_TASK_NONE,								//没有任何选项
					&err													//返回的错误码
				);
	//创建任务4
	OSTaskCreate(	(OS_TCB *)&Task4_TCB,									//任务控制块
					(CPU_CHAR *)"Task4",									//任务的名字
					(OS_TASK_PTR)task4,										//任务函数
					(void *)0,												//传递参数
					(OS_PRIO)6,											 	//任务的优先级7		
					(CPU_STK *)task4_stk,									//任务堆栈基地址
					(CPU_STK_SIZE)128/10,									//任务堆栈深度限位，用到这个位置，任务不能再继续使用
					(CPU_STK_SIZE)128,										//任务堆栈大小			
					(OS_MSG_QTY)0,											//禁止任务消息队列
					(OS_TICK)0,												//默认时间片长度																
					(void  *)0,												//不需要补充用户存储区
					(OS_OPT)OS_OPT_TASK_NONE,								//没有任何选项
					&err													//返回的错误码
				);
	//创建任务3
	OSTaskCreate(	(OS_TCB *)&Task5_TCB,									//任务控制块
					(CPU_CHAR *)"Task5",									//任务的名字
					(OS_TASK_PTR)task5,										//任务函数
					(void *)0,												//传递参数
					(OS_PRIO)6,											 	//任务的优先级7		
					(CPU_STK *)task5_stk,									//任务堆栈基地址
					(CPU_STK_SIZE)128/10,									//任务堆栈深度限位，用到这个位置，任务不能再继续使用
					(CPU_STK_SIZE)128,										//任务堆栈大小			
					(OS_MSG_QTY)0,											//禁止任务消息队列
					(OS_TICK)0,												//默认时间片长度																
					(void  *)0,												//不需要补充用户存储区
					(OS_OPT)OS_OPT_TASK_NONE,								//没有任何选项
					&err													//返回的错误码
				);


	//启动OS，进行任务调度
	OSStart(&err);
					
					
	printf("never run.......\r\n");
					
	while(1);
	
}


void task1(void *parg)
{

	
//	OS_ERR err;
	
	printf("task1 is create ok\r\n");

	while(1)
	{
		// CAN发送传感器数据
		
		delay_ms(10000);
	}
}

void task2(void *parg)
{

	
	printf("task2 is create ok\r\n");

	while(1)
	{
		if(flag==1)
		{		
			BEEP = 1;				//鸣叫提示接入成功
			delay_ms(250);
			BEEP = 0;
			
			printf("\r\nCAN接收到数据：\r\n");	
			printf("%s\r\n",RxMessage.Data);
			CAN_DEBUG_ARRAY(RxMessage.Data,8);
			if(strcmp("LED0_OFF",(char *)(RxMessage.Data))==0)
			{
				LED0 = 1;
			}
			if(strcmp("LED0__ON",(char *)(RxMessage.Data))==0)
			{
				LED0 = 0;
			}
			
			if(strcmp("LED1_OFF",(char *)(RxMessage.Data))==0)
			{
				LED1 = 1;
			}
			if(strcmp("LED1__ON",(char *)(RxMessage.Data))==0)
			{
				LED1 = 0;
			}
			
			if(strcmp("LED2_OFF",(char *)(RxMessage.Data))==0)
			{
				LED2 = 1;
			}
			if(strcmp("LED2__ON",(char *)(RxMessage.Data))==0)
			{
				LED2 = 0;
			}
			
			if(strcmp("BEEP_OFF",(char *)(RxMessage.Data))==0)
			{
				BEEP = 0;
			}
			if(strcmp("BEEP__ON",(char *)(RxMessage.Data))==0)
			{
				BEEP = 1;
			}
			
			flag=0;
		}
		delay_ms(500);
	}
}

void task3(void *parg)
{

	uint8_t key1flag = 0;
	uint8_t key2flag = 0;
	uint8_t key3flag = 0;
	uint8_t key4flag = 0;
	
	printf("task3 is create ok\r\n");

	while(1)
	{
		
		key_scan(0);	

		if(keydown_data==KEY0_DATA)   //key0按下后马上执行相应代码
		{
			if(key1flag == 0)
			{
				sprintf(can_sendbuf,"LED0__ON");
				CAN_SendMsg(&TxMessage,(uint8_t *)can_sendbuf,8);
				printf("CAN 控制LED0--开\r\n");
			}
			else
			{
				sprintf(can_sendbuf,"LED0_OFF");
				CAN_SendMsg(&TxMessage,(uint8_t *)can_sendbuf,8);
				printf("CAN 控制LED0--关\r\n");
			}
			
			key1flag++;
			if(key1flag == 2)
			{
				key1flag = 0;
			}
		 

		}
		if(keyup_data==KEY1_DATA)     //key1按下抬起之后执行相应代码
		{

			if(key2flag == 0)
			{
				sprintf(can_sendbuf,"LED1__ON");
				CAN_SendMsg(&TxMessage,(uint8_t *)can_sendbuf,8);
				printf("CAN 控制LED1--开\r\n");
			}
			else
			{
				sprintf(can_sendbuf,"LED1_OFF");
				CAN_SendMsg(&TxMessage,(uint8_t *)can_sendbuf,8);
				printf("CAN 控制LED1--关\r\n");
			}
			
			key2flag++;
			if(key2flag == 2)
			{
				key2flag = 0;
			}

		}
		
		if(keyup_data==KEY2_DATA)  //key2长按1秒后执行相应代码 由于延时5ms扫描一次按键所以5ms*200=1S
		{
			if(key3flag == 0)
			{
				sprintf(can_sendbuf,"LED2__ON");
				CAN_SendMsg(&TxMessage,(uint8_t *)can_sendbuf,8);
				printf("CAN 控制LED2--开\r\n");
			}
			else
			{
				sprintf(can_sendbuf,"LED2_OFF");
				CAN_SendMsg(&TxMessage,(uint8_t *)can_sendbuf,8);
				printf("CAN 控制LED2--关\r\n");
			}
			
			key3flag++;
			if(key3flag == 2)
			{
				key3flag = 0;
			}
			

		}
		if(keyup_data==KEY3_DATA)  //key3长按2秒后执行相应代码 由于延时5ms扫描一次按键所以5ms*400=2S
		{
			if(key4flag == 0)
			{
				sprintf(can_sendbuf,"BEEP__ON");
				CAN_SendMsg(&TxMessage,(uint8_t *)can_sendbuf,8);
				printf("CAN 控制BEEP--开\r\n");
			}
			else
			{
				sprintf(can_sendbuf,"BEEP_OFF");
				CAN_SendMsg(&TxMessage,(uint8_t *)can_sendbuf,8);
				printf("CAN 控制BEEP--关\r\n");
			}
			
			key4flag++;
			if(key4flag == 2)
			{
				key4flag = 0;
			}
		}
	
		delay_ms(50);
	}
}

void task4(void *parg)
{

	
	printf("task4 is create ok\r\n");

	while(1)
	{
		
		//printf("task4 is running ...\r\n");


		delay_ms(1000);
	}
}

void task5(void *parg)
{

	
	printf("task5 is create ok\r\n");

	while(1)
	{
		
		//printf("task5 is running ...\r\n");


		delay_ms(1000);
	}
}








