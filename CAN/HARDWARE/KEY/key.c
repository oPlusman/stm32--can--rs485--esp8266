#include "key.h"
#include "delay.h" 


/*
	按键初始化函数
*/

u8  keydown_data=0x00;    //按键按下后就返回的值
u8  keyup_data=0x00;      //按键抬起返回值
u16  key_time=0x00;       //按键按下之后的时间计数，该值乘以扫描一次按键函数的时间就等于按键按下的时间

u8  key_tem=0x00;         //长按的按键值与按键扫描程序过渡变量
u8  key_bak=0x00;         //按键扫描程序过渡变量

 /**
  * @brief  初始化控制KEY的IO
  * @param  无
  * @retval 无
  */
void KEY_Init(void)
{		
	/*定义一个GPIO_InitTypeDef类型的结构体*/
	GPIO_InitTypeDef GPIO_InitStructure;

	/*开启KEY相关的GPIO外设时钟*/
	RCC_AHB1PeriphClockCmd ( KEY0_GPIO_CLK|
							KEY1_GPIO_CLK|
							KEY2_GPIO_CLK|
							KEY3_GPIO_CLK, ENABLE); 

	/*选择要控制的GPIO引脚*/															   
	GPIO_InitStructure.GPIO_Pin = KEY0_PIN;	

	/*设置引脚模式为输入模式*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;   

//	/*设置引脚的输出类型为推挽输出*/
//	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;

	/*设置引脚为上拉模式*/
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

	/*设置引脚速率为100MHz */   
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz; 

	/*调用库函数，使用上面配置的GPIO_InitStructure初始化GPIO*/
	GPIO_Init(KEY0_GPIO_PORT, &GPIO_InitStructure);	

	/*选择要控制的GPIO引脚*/															   
	GPIO_InitStructure.GPIO_Pin = KEY1_PIN;	
	GPIO_Init(KEY1_GPIO_PORT, &GPIO_InitStructure);	

	/*选择要控制的GPIO引脚*/															   
	GPIO_InitStructure.GPIO_Pin = KEY2_PIN;	
	GPIO_Init(KEY2_GPIO_PORT, &GPIO_InitStructure);	
	
	/*选择要控制的GPIO引脚*/															   
	GPIO_InitStructure.GPIO_Pin = KEY3_PIN;	
	GPIO_Init(KEY3_GPIO_PORT, &GPIO_InitStructure);	

}

/****************************************************************************
* 名    称: void key_scan(u8 mode)
* 功    能：按键扫描函数
* 入口参数：mode：0：单按 
                  1: 连按
* 返回参数：无
* 说    明：响应优先级,KEY0>KEY1>KEY2>KEY3
****************************************************************************/
void key_scan(u8 mode)
{	   
	 keyup_data=0;         //键抬起后按键值一次有效
	
	if(KEY0==0||KEY1==0||KEY2==0||KEY3==0)   //有键正按下
	{
		if(KEY0==0)      key_tem=1;
		else if(KEY1==0) key_tem=2;
		else if(KEY2==0) key_tem=3;
		else if(KEY3==0) key_tem=4;
		
		if (key_tem == key_bak)      //有键按下后第一次扫描不处理，与else配合第二次扫描有效，这样实现了去抖动
		{
			key_time++;             //有键按下后执行一次扫描函数，该变量加1
			keydown_data=key_tem;   //按键值赋予keydown_data
					
			if( (mode==0)&&(key_time>1) )//key_time>1按键值无效，这就是单按，如果mode为1就为连按
				keydown_data=0;
       	}
	    else                             //去抖动      
	    {
		       key_time=0;
		       key_bak=key_tem;
	    }
		
	}
	else       //键抬起
	{
		if(key_time>2)            //按键抬起后返回一次按键值
		{
			keyup_data=key_tem;  //键抬起后按键值赋予keydown_data            						
		}
		key_bak=0;               //要清零，不然下次执行扫描程序时按键的值跟上次按的值一样，就没有去抖动处理了
		key_time=0;
		keydown_data=0;		
	 }    
}


