#ifndef _ESP8266_H_
#define _ESP8266_H_

#include "stdio.h"	
#include "stdarg.h"



#define REV_OK		0	//接收完成标志
#define REV_WAIT	1	//接收未完成标志

#define   ESP8266_CH_PD_APBxClock_FUN    RCC_AHB1PeriphClockCmd
#define   ESP8266_CH_PD_CLK              RCC_AHB1Periph_GPIOE 
#define   ESP8266_CH_PD_PORT             GPIOE
#define   ESP8266_CH_PD_PIN              GPIO_Pin_11

#define   ESP8266_RST_APBxClock_FUN      RCC_AHB1PeriphClockCmd
#define   ESP8266_RST_CLK                RCC_AHB1Periph_GPIOE
#define   ESP8266_RST_PORT               GPIOE
#define   ESP8266_RST_PIN                GPIO_Pin_8


#define     ESP8266_CH_ENABLE()        GPIO_SetBits ( ESP8266_CH_PD_PORT, ESP8266_CH_PD_PIN )
#define     ESP8266_CH_DISABLE()       GPIO_ResetBits ( ESP8266_CH_PD_PORT, ESP8266_CH_PD_PIN )

#define     ESP8266_RST_HIGH_LEVEL()   GPIO_SetBits ( ESP8266_RST_PORT, ESP8266_RST_PIN )
#define     ESP8266_RST_LOW_LEVEL()    GPIO_ResetBits ( ESP8266_RST_PORT, ESP8266_RST_PIN )



void ESP8266_Init(void);

void ESP8266_Clear(void);

void ESP8266_SendData(unsigned char *data, unsigned short len);

unsigned char *ESP8266_GetIPD(unsigned short timeOut);



void Uart5_Init(u32 baud);

void Usart_SendString(USART_TypeDef *USARTx, unsigned char *str, unsigned short len);

void UsartPrintf(USART_TypeDef *USARTx, char *fmt,...);


#endif
