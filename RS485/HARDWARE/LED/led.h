#ifndef __LED_H
#define __LED_H

#include "sys.h"


//引脚定义
/*******************************************************/
//LED0
#define LED0_PIN                  GPIO_Pin_3                 
#define LED0_GPIO_PORT            GPIOE                      
#define LED0_GPIO_CLK             RCC_AHB1Periph_GPIOE

//LED1
#define LED1_PIN                  GPIO_Pin_4                 
#define LED1_GPIO_PORT            GPIOE                 
#define LED1_GPIO_CLK             RCC_AHB1Periph_GPIOE

//LED2
#define LED2_PIN                  GPIO_Pin_9                 
#define LED2_GPIO_PORT            GPIOG                  
#define LED2_GPIO_CLK             RCC_AHB1Periph_GPIOG
/************************************************************/


//LED端口定义
#define LED0 PEout(3)	
#define LED1 PEout(4)		
#define LED2 PGout(9)	

void LED_Init(void);//初始化		 				    
#endif
