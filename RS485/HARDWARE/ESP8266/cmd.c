#include "cmd.h"

#include <stdio.h>  
#include <string.h>  
#include <stdbool.h>
#include <stdlib.h>

#include "led.h"
#include "beep.h"


/**
  * @brief  获取网络调试助手和串口调试助手发来的信息
  * @param  如ESP8266的返回格式为	"+IPD,x:yyy" x代表数据长度，yyy是数据内容
  * @retval 无
  */
void Get_ESP82666_Cmd(unsigned char * cmd )
{
	uint8_t	i=0;
	char *p;
	
	// 数据发送控制命令 +IPD,x:板子编号LED-0-ON#
	if(strstr((char *)cmd,"01LED"))
	{
		//以等号分割字符串
		strtok((char *)cmd,":-#"); //+IPD,x
		
		p=strtok(NULL,":-#"); // 01LED
		//获取状态
		p=strtok(NULL,"-#");
		i = atoi(p);
		if(i==0)
		{
			p=strtok(NULL,"-#"); //ON OFF
			if(strcmp(p, "OFF") == 0)
			{
				LED0 = 1;
			}
			if(strcmp(p, "ON") == 0)
			{
				LED0 = 0;
			}
		}
		if(i==1)
		{
			p=strtok(NULL,"-#");
			if(strcmp(p, "OFF") == 0)
			{
				LED1 = 1;
			}
			if(strcmp(p, "ON") == 0)
			{
				LED1 = 0;
			}
		}
		if(i==2)
		{
			p=strtok(NULL,"-#");
			if(strcmp(p, "OFF") == 0)
			{
				LED2 = 1;
			}
			if(strcmp(p, "ON") == 0)
			{
				LED2 = 0;
			}
		}
		
	}
	
	// 数据发送控制命令 +IPD,x:板子编号BEEP-ON#
	//示例：BEEP-ON# BEEP-OFF# BEEP ON
	//蜂鸣器控制
	if(strstr((char *)cmd,"01BEEP"))
	{
		//以等号分割字符串
		strtok((char *)cmd,":-#"); //+IPD,x
		
		p=strtok(NULL,":-#"); //01BEEP
		
		//获取状态
		p=strtok(NULL,":-#");
		if(strcmp(p, "ON") == 0)
		{
			BEEP = 1;
		}
		if(strcmp(p, "OFF") == 0)
		{
			BEEP = 0;
		}
	}	
	
}

